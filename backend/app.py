"""meh"""

import bcrypt
import json

from flask import jsonify, request, render_template, redirect
from flask_jwt import JWT, jwt_required, current_identity

from maturidade_digital.app_factory import app_instance
from maturidade_digital.models import (
    User, Answer, AnsweredRevisions, Client, Revision, Question, Questionaire
)


def partitiont(list, p, f):
    """
    Partition transform.
    The left-side `a` is the list of items
    that has passed the predicate `p`,
    otherwise right-side `b`.

    `f` is the transform function that is applied
    to every item.
    """
    a, b = [], []
    for i in list:
        (a if p(i) else b).append(f(i))
    return a, b


# Because there is no sense...
app, db = app_instance()


def authenticate(username, password):
    user = User.query.filter(User.username == username).scalar()
    valid = bcrypt.checkpw(password.encode('ascii'),
                           user.password.encode('ascii'))
    return user if valid else None


def identity(payload):
    return User.query.filter(User.id == payload['identity']).scalar()


jwt = JWT(app, authenticate, identity)

db.create_all()


def transaction(session, f):
    res = f(session)
    session.commit()
    return res


ANSWER_OPTIONS = [
    {"id": 0, "name": "Completamente Desconhecido"},
    {"id": 1, "name": "Conheço Pouco"},
    {"Id": 2, "name": "Conheço Parcialmente"},
    {"Id": 3, "name": "Conheço"},
    {"Id": 4, "name": "Conheço Totalmente"}
]


@jwt.jwt_error_handler
def error_handler(e):
    return e.description, e.status_code


@app.route('/api/clients', methods=['GET'])
@jwt_required()
def list_clients():
    return jsonify([c.to_dict() for c in Client.list_all()])


@app.route('/api/client', methods=['POST'])
@jwt_required()
def register_client():
    data = json.loads(request.data)

    def work(session):
        client = Client(**data)
        session.add(client)
        return client

    client = transaction(db.session, work)
    return client.to_dict()


@app.route('/api/client/<int:id>', methods=['GET'])
@jwt_required()
def retrieve_client(id):
    client = Client.by_id(id)

    if not client:
        return "Not found", 404

    ar = Questionaire.revisions_by_client(db.session, id)

    rules = ("-questions", "-clients", "-revisions")

    p = lambda a: a.answered == True
    def t(a):
        return {"answered": a.answered,
                "revision_id": a.id,
                "link": a.link}

    answered_revisions, revisions = partitiont(ar, p, t)

    return jsonify({
        "client": client.to_dict(),
        "answered": answered_revisions,
        "revisions": revisions
    })


@app.route('/api/client/<int:client_id>/revision/<int:revision_id>', methods=['POST'])
@jwt_required()
def generate_revision_link_for_client(client_id, revision_id):
    client = Client.by_id(client_id)

    if not client:
        return "meh", 500

    revision = Revision.by_id(revision_id)

    if not revision:
        return "meh", 500


    def work(session):
        from uuid import uuid4 as uuid
        link = str(uuid()).replace('-', '')
        answered_revision = AnsweredRevisions(
            revision_id=revision_id,
            client_id=client_id,
            link=link
        )
        session.add(answered_revision)
        return answered_revision

    answered_revision = transaction(db.session, work)

    return jsonify(answered_revision.to_dict(rules=("-clients", "-revisions")))


@app.route('/api/revisions', methods=['GET'])
@jwt_required()
def list_revisions():
    revisions = Revision.list_all()
    return jsonify([r.to_dict(rules=("-questions",)) for r in revisions])


@app.route('/api/revision/<int:id>', methods=['GET'])
@jwt_required()
def revision_by_id(id):
    revision = Revision.by_id(id)

    if not revision:
        return "Not found", 404

    return jsonify(revision.to_dict())


@app.route('/api/revision', methods=['POST'])
@jwt_required()
def register_revision():
    questions = json.loads(request.data)

    if not questions or len(questions) == 0:
        return jsonify({
            "error": {"message": "Need at least one question."}
        })

    revision = Revision(description="")

    transaction(db.session, lambda session: session.add(revision))

    revision_id = revision.id

    work = lambda session: session.add_all(
        [Question(revision_id=revision_id, **q) for q in questions]
    )
    transaction(db.session, work)

    return jsonify({"id": revision_id,
                    "description": revision.description})


@app.route('/api/answers/<string:link>', methods=['GET'])
@jwt_required()
def retrieve_clients_answers_for_revision(link):
    client = AnsweredRevisions.client_of_link(db.session, link)
    answers = Questionaire.client_answers_by_link(db.session, link)

    return jsonify({
        "client": client.to_dict(),
        "answers": answers
    })


@app.route('/api/questionaire/<string:link>/answers', methods=['GET'])
@jwt_required()
def questionaire_answers(link):
    return Questionaire.client_answers_by_link(db.session, link)


@app.route('/api/questionaire/<string:link>', methods=['GET'])
def questionaire_by_link(link):
    revision = Questionaire.by_link(db.session, link)

    if not revision:
        return "Not found", 404

    return jsonify({"revision": revision,
                    "answer_options": ANSWER_OPTIONS })


@app.route('/api/questionaire/<string:link>/answers', methods=['POST'])
def submitted_answers(link):
    revision = AnsweredRevisions.by_link(link)

    if not revision:
        return "Not found", 404

    if revision.answered:
        return jsonify({
            "error": {"message": "Already answered this revision."}
        })

    answers = json.loads(request.data)

    if len(answers) == 0:
        return jsonify({
            "error": {"message": "Answers cannot be empty."}
        })

    def work(session):
        revision.answered = True
        session.add(revision)

        session.add_all(
            [Answer(**a) for a in answers]
        )

    transaction(db.session, work)

    return "Done"
