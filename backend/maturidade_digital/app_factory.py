import os
from datetime import timedelta

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask('maturidade_digital')

DEBUG = os.getenv('DEBUG') == "True"
SQLALCHEMY_ECHO = DEBUG
SQLALCHEMY_TRACK_MODIFICATIONS = DEBUG
SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI') 
SECRET_KEY = os.getenv('SECRET_KEY') 
JWT_AUTH_URL_RULE = os.getenv('JWT_AUTH_URL_RULE') 
JWT_AUTH_USERNAME_KEY = os.getenv('JWT_AUTH_USERNAME_KEY') 
JWT_AUTH_PASSWORD_KEY = os.getenv('JWT_AUTH_PASSWORD_KEY') 
JWT_EXPIRATION_DELTA = os.getenv('JWT_EXPIRATION_DELTA') 

app.config['SQLALCHEMY_ECHO'] = SQLALCHEMY_ECHO
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS
app.config['DEBUG'] = DEBUG
app.config['SECRET_KEY'] = SECRET_KEY
app.config['JWT_AUTH_URL_RULE'] = JWT_AUTH_URL_RULE
app.config['JWT_AUTH_USERNAME_KEY'] = JWT_AUTH_USERNAME_KEY
app.config['JWT_AUTH_PASSWORD_KEY'] = JWT_AUTH_PASSWORD_KEY
app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=3600 * 24)


db = SQLAlchemy(app)

def app_instance():
    return app, db
