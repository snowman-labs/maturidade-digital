"""meh"""

from dataclasses import dataclass
from sqlalchemy import Column, Boolean, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, joinedload
from sqlalchemy_serializer import SerializerMixin
from .app_factory import app_instance

app, db = app_instance()


@dataclass
class User(db.Model, SerializerMixin):
    __tablename__ = 'user_credentials'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    username = Column(String)
    password = Column(String)


@dataclass
class Client(db.Model, SerializerMixin):
    __tablename__ = 'client'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
    phone_number = Column(String)
    demand = Column(String)

    @classmethod
    def list_all(cls):
        return cls.query.all()

    @classmethod
    def by_id(cls, id):
        return cls.query.get(id)


@dataclass
class Question(db.Model, SerializerMixin):
    __tablename__ = 'question'

    id = Column(Integer, primary_key=True)
    revision_id = Column(Integer, ForeignKey('revision.id'))
    description = Column(String)
    scale = Column(Integer)


@dataclass
class Revision(db.Model, SerializerMixin):
    __tablename__ = 'revision'

    id = Column(Integer, primary_key=True)
    description = Column(String, default=None)
    questions = relationship('Question',
                             primaryjoin=(Question.revision_id == id))

    @classmethod
    def by_id(cls, id):
        return cls.query.get(id)

    @classmethod
    def list_all(cls):
        return cls.query.all()

    @classmethod
    def list_unanswered_by_client(cls, ids):
        return Revision.query.filter(~cls.id.in_(ids)).all()


@dataclass
class AnsweredRevisions(db.Model, SerializerMixin):
    __tablename__ = 'answered_revisions'

    id = Column(Integer, primary_key=True)
    client_id = Column(Integer, ForeignKey('client.id'))
    client = relationship('Client')
    revision_id = Column(Integer, ForeignKey('revision.id'))
    revisions = relationship('Revision',
                             primaryjoin=(Revision.id == revision_id))

    answered = Column(Boolean, default=False)
    link = Column(String)

    @classmethod
    def by_client(cls, id):
        return cls.query.filter(cls.client_id==id).all()

    @classmethod
    def by_link(cls, link):
        return cls.query.filter(cls.link==link).first()

    @classmethod
    def client_of_link(cls, session, link):
        subquery = db.session.query(cls.client_id).filter(cls.link == link)
        return Client.query.filter(Client.id==subquery).first()


@dataclass
class Answer(db.Model, SerializerMixin):
    __tablename__ = 'answer'

    id = Column(Integer, primary_key=True)
    revision_id = Column(Integer, ForeignKey('revision.id'))
    question_id = Column(Integer, ForeignKey('question.id'))
    question = relationship('Question',
                            primaryjoin=(Question.id==question_id))
    score = Column(Integer, default=0)


# TODO(dias): use a repository...
class Questionaire():
    @classmethod
    def latest_questionaire(cls, session):
        last_revision = session.query(Revision.id). \
            order_by(Revision.id.desc()).limit(1)
        return cls.by_revision(session, last_revision)

    @classmethod
    def by_revision(cls, session, id):
        return Revision.query.options(
            joinedload(Revision.questions)
        ).filter(Revision.id == id).first()

    @classmethod
    def by_link(cls, session, link):
        from functools import reduce

        A = AnsweredRevisions
        R = Revision

        data = session.query(A.id, A.client_id, Question).join(
            Question, Question.revision_id==A.revision_id
        ).filter(A.link == link).all()

        (id, client_id, question) = data[0]

        return { "id": id,
                 "client_id": client_id,
                 "questions": list(map(lambda line: line[2].to_dict(), data))
                }

    @classmethod
    def client_answers_by_link(cls, session, link):
        AR = AnsweredRevisions
        Q = Question
        A = Answer

        subquery = db.session.query(AR.revision_id). \
            filter(AR.link == link)

        data = db.session.query(A.revision_id, A.question_id, A.score,
                                Q.description, Q.scale). \
            join(Q, Q.id == A.question_id). \
            filter(A.revision_id == subquery.subquery()).all()

        def transform(line):
            return {"revision_id": line[0],
                    "question_id": line[1],
                    "score": line[2],
                    "description": line[3],
                    "scale": line[4]}

        return list(map(transform, data))

    @classmethod
    def revisions_by_client(cls, session, client_id):
        from sqlalchemy.sql.expression import and_, case

        A = AnsweredRevisions
        R = Revision

        require_link = case([(A.link == None, '',)], else_=A.link).label("link")

        return session.query(R.id, A.answered, require_link). \
            outerjoin(A, and_(R.id == A.revision_id, A.client_id == client_id)). \
            all()
