#!/bin/sh

export DATABASE_URI="sqlite:////var/database/md.db"

exec gunicorn -w 4 --bind=0.0.0.0:80 app:app
