import "../src/main.css";
import { app } from "hyperapp";
import { div } from "@hyperapp/html";
import withRouter from '@mrbarrysoftware/hyperapp-router';
import { redirectAuthed, blockAccess } from "dmd/lib/login.js";
import { redirect } from "dmd/lib/navigation.js";
import ApiService from "dmd/services/api";
import HomeView from "dmd/views/home.js";
import LoginView from "dmd/views/login.js";
import ClientsView from "dmd/views/clients.js";
import ClientView from "dmd/views/client.js";
import AnswerView from "dmd/views/answer.js";
import RevisionsView from "dmd/views/revisions.js";
import RevisionView from "dmd/views/revision.js";

const router = {
  baseUrl: '/admin',

  RouteAction: (state, { path: url }) => ({ ...state, url }),

  routes: {
    [HomeView.route]: HomeView.routeSpec,
    [LoginView.route]: LoginView.routeSpec,
    [ClientsView.route]: ClientsView.routeSpec,
    [ClientView.route]: ClientView.routeSpec,
    [AnswerView.route]: AnswerView.routeSpec,
    [RevisionsView.route]: RevisionsView.routeSpec,
    [RevisionView.route]: RevisionView.routeSpec
  }
};

const emptyView = () => div();

const LogoutFx = (state) => [
  ({ ...state, api: null, state: null }),
  redirect("/admin/login")
];

const LogoutSub = (dispatch) => {
  const target = document.body;
  const handler = (ev) =>  dispatch([LogoutFx, ev.key]);

  target.addEventListener("logout", handler);

  return () => target.removeEventListener("logout", handler);
}

withRouter(app)({
  router,

  subscriptions: () => [
    [LogoutSub, {}]
  ],

  init: {
    url: '',
    api: (function() {
      const token = localStorage.getItem("token");
      return token && new ApiService(token);
    }()),
    verifyAuthorization: (state, next) => {
      if (blockAccess(state.api, state.url)) {
	return [state, redirect("/admin/login")];
      }

      if (redirectAuthed(state.api, state.url)) {
	return [state, redirect("/admin")];
      }

      return next(state);
    },
    view: emptyView
  },

  view: state => state.view(state),

  node: document.getElementById("app")
});
