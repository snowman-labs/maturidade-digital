import "../src/main.css";
import axios from "axios";
import { h, text, app } from "hyperapp";
import { div } from "@hyperapp/html";
import withRouter from '@mrbarrysoftware/hyperapp-router';
import { redirectAuthed, blockAccess } from "dmd/lib/login.js";
import { redirect } from "dmd/lib/navigation.js";
import FormView from "dmd/views/form.js";
import ResultsView from "dmd/views/results.js";


const emptyView = () => div();

const router = {
  RouteAction: (state, { path: url }) => ({ ...state, url }),

  routes: {
    [FormView.route]: FormView.routeSpec,
    [ResultsView.route]: ResultsView.routeSpec
  }
};

withRouter(app)({
  router,

  init: {
    url: '',
    api: axios.create({
      baseURL: "http://localhost:5000/api"
    }),
    view: emptyView
  },

  view: state => state.view(state),

  node: document.getElementById("app")
});
