import { div, text } from "@hyperapp/html";
import navigation from "dmd/components/navigation.js";

export const page = (state, ...page) => div({}, [
  navigation(),
  div(
    { class: "mx-16 my-10" },
    page
  )
]);

export const answerPage = (state, ...page) => div(
  { class: "mx-16 my-10" },
  page
);


export const login = (state, ...page) => div(
  { class: "mx-16 my-10" },
  page
);
