import { h1, h2, h4, div, text, button } from "@hyperapp/html";

export const title = (props, label) => (
  h1({ ...props, class: `text-4xl font-bold ${props.class || ""}` }, text(label))
);

export const subtitle = (props, label) => (
  h2({ ...props, class: `text-2xl font-bold ${props.class || ""}` }, text(label))
);

export const propheading = (props, label) => (
  h4({ ...props, class: `text-sm font-bold ${props.class || ""}` }, text(label))
);

export const halfview = (props, a,b) => (
  div(
    { ...props, class: `grid grid-cols-2 gap-4 ${props.class || ""}` },
    [a,b]
  )
);

export const submitbutton = (props, label) => (
  button(
    {
      type: "submit",
      class: `rounded bg-green-600 text-white px-6 py-2 ${props.class || ""}`
    },
    text(label)
  )
);
