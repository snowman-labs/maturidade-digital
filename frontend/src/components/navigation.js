import { nav, a, text } from "@hyperapp/html";
import { goto } from "../lib/navigation.js";

const logo = () => img({ src: "" });

const navitems = [
  ["Clients", "/admin/clients"],
  ["Revisions", "/admin/revisions"]
];

export default (state) => nav(
  { class: "bg-blue-900 px-10 py-6" },
  navitems.map(
    ([label, href]) => a({ class: "text-white px-6", href }, text(label))
  )
);
