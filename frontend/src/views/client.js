import { ul, li, a, p, div, text, form } from "@hyperapp/html";
import { halfview, title, propheading } from "dmd/components/index.js";
import { page } from "dmd/layout/page.js";

export const ClientState = (client, revisionId) => ({ ...client, revisionId });

export const Client = (state, client) => ({
  ...state,
  state: ClientState(client, null)
});

export const MoveRevisionToAnswered = (s, answeredRevision) => {
  const revisionId = answeredRevision.revision_id;
  const _revisions = s.state.revisions.filter(
    ({ revision_id }) => revision_id != revisionId
  );
  const revisions = [..._revisions, answeredRevision];
  return {
    ...s,
    state: ClientState({ ...s.state, revisions }, null)
  }
};

export const ClientFx = (dispatch, { api, id }) => (
  api.clientById(id).then(
    ({ data: client }) => dispatch([Client, client])
  ).catch(
    error => { console.error(error) }
  )
);

export const GenerateRevisionLinkFx = (dispatch, { api, clientId, revisionId }) => (
  api.generateLink(clientId, revisionId).then(
    ({ data }) => dispatch([MoveRevisionToAnswered, data])
  )
);

export const GenerateRevisionLink = (revisionId) => s => [
  s,
  [GenerateRevisionLinkFx, {
    api: s.api,
    clientId: s.state.client.id,
    revisionId
  }]
];

export const displayProp = (label, value) => div(
  { class: "mb-4" },
  [
    propheading({},  label),
    p({}, text(value))
  ]
);

export const displayClient = (client) => client && (
  div ({}, [
    title({ class: "mb-4" }, "Client"),
    displayProp("Name", client.name),
    displayProp("E-mail", client.email),
    displayProp("Telefone", client.phone_numer),
    displayProp("Demanda", client.demanda)
  ])
);

export const revisionLink = revision => (
  (revision.link || "").length > 0 ?
    a({ href: `/${revision.link}` }, text("Link generated")) :
    a({
      class: "rounded bg-blue-600 text-white px-6 py-2",
      onclick: GenerateRevisionLink(revision.revision_id)
    }, text("Gerar link"))
);

export const generateLinkForRevision = revisions => revisions && (
  form(
    {},
    [
      title({ class: "mb-4" }, "Generate link for revision"),
      div(
	{},
	revisions.map (
	  revision => div(
	    { class: "w-full flex justify-between mb-4" },
	    [
	      p({}, text(`Revision: ${revision.revision_id}`)),
	      revisionLink(revision)
	    ]
	  )
	)
      )
    ]
  )
);

export const listAnsweredRevisions = (client, answeredRevisions) => (
  answeredRevisions && answeredRevisions.map(
    answeredRevision => li(
      a({
	href: `/admin/client/${client.id}/answers/${answeredRevision.link}`
      }, text(answeredRevision.revision_id.toString()))
    )
  )
);

export const displayAnsweredRevisions = (client, answeredRevisions) => (
  answeredRevisions && div(
    {}, [
      title({ class: "mb-4" }, "Answered revisions"),
      ul({}, listAnsweredRevisions(client, answeredRevisions))
    ]
  )
);

export const view = (s) => page(
  s,
  halfview(
    {},
    displayClient(s.state.client),
    div(
      {},
      [
	generateLinkForRevision(s.state.revisions),
	displayAnsweredRevisions(s.state.client, s.state.answered)
      ]
    )
  )
);

view.route = '/client/:id';
view.routeSpec = {
  OnEnter: (state, { id }) => {
    return state.verifyAuthorization(state, (state) => [
      { ...state, view, state: ClientState(null, null) },
      [ClientFx, { api: state.api, id: parseInt(id) }]
    ]);
  }
};

export default view;
