import { ul, li, a, div, text, input, form, button } from "@hyperapp/html";
import { title, halfview, submitbutton } from "dmd/components/index.js";
import { goto } from "dmd/lib/navigation.js";
import { page } from "dmd/layout/page.js";

export const RevisionsState = (revisions, newRevision) => ({
  revisions,
  newRevision
});

export const Revisions = (s, revisions) => ({
  ...s,
  state: RevisionsState(revisions, [])
})

export const RevisionsFx = (dispatch, { api }) => (
  api.revisions().then(
    ({data}) => dispatch([Revisions, data])
  )
);

export const AddQuestion = (s) => ({
  ...s,
  state: RevisionsState(s.state.revisions, [
    ...s.state.newRevision, {
      description: "",
      scale: 1
    }
  ])
});

export const RemoveQuestion = (index) => s => {
  const { newRevision } = s.state;
  let update = newRevision.slice(0, index).concat(
    newRevision.slice(index + 1, newRevision.length)
  );
  return {
    ...s,
    state: RevisionsState(s.state.revisions, update)
  };
};

export const UpdateQuestionField = (index) => (s, event) => {
  const { newRevision } = s.state;
  newRevision[index][event.target.name] = event.target.value;
  return { ...s };
};

export const listRevisions = (revisions) => revisions && ul(
  {},
  revisions.map(
    ({ id }) => li(
      { class: "mb-1" },
      a({ href: `/admin/revision/${id}` }, text(`Revision: ${id}`))
    )
  )
);

export const displayRevisions = (revisions) => div({}, [
  title({ class: "mb-4" }, "Revisions"),
  listRevisions(revisions)
]);

export const questioninput = (index, { description, scale }) => div({
  class: "flex mb-4"
}, [
  input ({
    type: "text",
    class: "rounded border border-gray-200 px-4 py-2",
    name: "description",
    value: description,
    onchange: UpdateQuestionField(index),
    placeholder: "Question"
  }),
  input ({
    type: "number",
    name: "scale",
    class: "rounded border border-gray-200 px-4 py-2",
    onchange: UpdateQuestionField(index),
    value: scale
  }),
  button({
    type: "button",
    class: `rounded bg-red-600 text-white px-4 py-2`,
    onclick: RemoveQuestion(index)
  }, text("Remove"))
])

export const IncludeRevision = (s, revision) => ({
  ...s,
  state: RevisionsState([...s.state.revisions, revision], [])
});

export const SubmitRevisionFx = (dispatch, { api, revision }) => (
  api.registerRevision(revision).then(
    ({ data }) => dispatch([IncludeRevision, data])
  )
);

export const SubmitRevision = (s, event) => {
  event.preventDefault();
  return [
    s,
    [SubmitRevisionFx, { api: s.api, revision: s.state.newRevision }]
  ];
};


export const createRevision = (newRevision) => div ({}, [
  title ({ class: "mb-4" }, "Create revision"),
  form({ onsubmit: SubmitRevision }, [
    button({
      type: "button",
      class: `rounded border border-gray-200 text-blue px-4 py-2 mb-4`,
      onclick: AddQuestion
    }, text("Add question")),
    div(
      {},
      newRevision.map (
	(revision, index) => questioninput(index, revision)
      )
    ),
    submitbutton({}, "Send")
  ])
]);

export const view = s => page(
  s,
  halfview(
    {},
    displayRevisions(s.state.revisions),
    createRevision(s.state.newRevision)
  )
);

view.route = '/revisions';
view.routeSpec = {
  OnEnter: (state, params) => state.verifyAuthorization(
    state,
    () => [
      { ...state, view, state: RevisionsState([], []) },
      [RevisionsFx, { api: state.api }]
    ]
  )
};

export default view;
