import axios from "axios";
import { form, input } from "@hyperapp/html";
import { title,  submitbutton } from "dmd/components/index.js";
import { redirect } from "dmd/lib/navigation.js";
import { login } from "dmd/layout/page.js";
import ApiService from "dmd/services/api";

export const Auth = (state, props) => {
  localStorage.setItem('token', props);
  const api = props && new ApiService(props);
  return [{ ...state, api }, redirect("/admin/")];
};

export const UpdateUsername = (state, event) => ({
  ...state,
  state: LoginState(event.target.value, state.state.password)
});

export const UpdatePassword = (state, event) => ({
  ...state,
  state: LoginState(state.state.username, event.target.value)
});

export const SubmitLoginFx = (dispatch, props) => {
  ApiService.authenticate(props).then(
    ({ data: { access_token } }) => dispatch([Auth, access_token])
  );
};

export const SubmitLogin = (state, event) => {
  event.preventDefault();
  return [state, [SubmitLoginFx, state.state]];
};

export const LoginState = (username, password) => ({ username, password });

export const view = state => {
  const { username, password } = state.state;
  return login(
    state,
    title({ class: "text-blue font-bold mb-4" }, "Login"),
    form({ onsubmit: SubmitLogin }, [
      input({
	class: "w-full rounded border border-gray-200 px-10 py-6 mb-4",
	type: "text", placeholder: "username",
	value: username, onchange: UpdateUsername
      }),
      input({
	class: "w-full rounded border border-gray-200 px-10 py-6 mb-4",
	type: "password", placeholder: "password",
	value: password, onchange: UpdatePassword
      }),
      submitbutton({}, "Send")
    ])
  );
};

view.route = '/login';
view.routeSpec = {
  OnEnter: (state) => {
    localStorage.removeItem("token");
    return {
      ...state,
      api: null,
      view,
      state: LoginState("", "")
    };
  }
};

export default view;
