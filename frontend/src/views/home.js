import { div, text } from "@hyperapp/html";
import { page } from "dmd/layout/page.js";

const view = (state) => page(state, null);

view.route = '/';
view.routeSpec = {
  OnEnter: (state, params) => state.verifyAuthorization(
    state,
    state => ({ ...state, view })
  )
};

export default view;
