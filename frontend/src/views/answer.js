import { p, div, text } from "@hyperapp/html";
import { halfview, title, subtitle, propheading } from "dmd/components/index.js";
import { page } from "dmd/layout/page.js";


export const AnswerState = (state, answers) => ({ ...state, state: answers });

export const AnswerFx = (dispatch, {api, client_id, link}) => (
  api.answeredQuestionaireByLink(link).then (
    ({ data }) => dispatch([AnswerState, data])
  )
);

export const listAnswers = answers => answers && answers.map(
  answer => div(
    { class: "mb-4" },
    [
      propheading({}, answer.description),
      p({}, text((answer.scale * answer.score / 100).toString()))
    ]
  )
);

const clientNameOrDefault = (client, def) => (client || { name: def}).name;

export const view = s => page(
  s,
  title({ class: "mb-4" }, "Client"),
  p({ class: "mb-4" }, text(`Name: ${clientNameOrDefault(s.state.client, "")}`)),
  title({ class: "mb-4" }, "Answer"),
  div({}, listAnswers(s.state.answers))
);

view.route = '/client/:id/answers/:link';
view.routeSpec = {
  OnEnter: (state, { id, link }) => {
    return state.verifyAuthorization(state, (state) => [
      { ...state, view, state: AnswerState(null) },
      [AnswerFx, {
	api: state.api,
	client_id: parseInt(id),
	link
      }]
    ]);
  }
};

export default view;
