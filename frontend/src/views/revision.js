import { p, blockquote, div, text } from "@hyperapp/html";
import { title, propheading } from "dmd/components/index.js";
import { page } from "dmd/layout/page.js";

export const RevisionState = (revision) => ({ revision });

export const Revision = (s, revision) => ({
  ...s,
  state: RevisionState(revision)
});

export const RevisionFx = (dispatch, { api, revisionId }) => (
  api.revisionById(revisionId).then(
    ({ data }) => dispatch([Revision, data])
  )
);

export const displayRevision = (revision) => revision && div(
  {},
  [
    p({ class: "mb-4" }, text(revision.id.toString())),
    propheading({ class: "mb-2" }, "Questions"),
    div(
      {},
      revision.questions.reduce(
	(acc, { description, scale }) => acc.concat([
	  blockquote({}, text(`"${description}"`)),
	  p({ class: "text-xs mb-4" }, text(`Scale: ${scale}`))
	]),
	[]
      )
    )
  ]
);

export const view = s => page(
  s,
  title({}, "Revision"),
  displayRevision(s.state.revision)
);

view.route = '/revision/:id';
view.routeSpec = {
  OnEnter: (state, { id }) => state.verifyAuthorization(
    state,
    () => [
      { ...state, view, state: RevisionState(null) },
      [RevisionFx, { api: state.api, revisionId: parseInt(id) }]
    ]
  )
};

export default view;
