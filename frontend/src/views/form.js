import { ul, li, a, p, span, div, text, input, form } from "@hyperapp/html";
import { halfview, title, propheading, submitbutton } from "dmd/components/index.js";
import { answerPage } from "dmd/layout/page.js";
import { redirect } from "dmd/lib/navigation.js";
import ApiService from "dmd/services/api";

export const FormState = (link, revision, form) => ({
  ...revision,
  link,
  form
});

export const Form = (s, revision) => ({
  ...s,
  state: FormState(s.state.link, revision, {})
});

export const FormFx = (dispatch, { api, form ,link }) => (
  ApiService.questionaireByLink(link).then(
    ({ data }) => dispatch([Form, data])
  )
);

export const SelectedAnswer = (question, optionId) => s => ({
  ...s,
  state: {
    ...s.state,
    form: {
      ...s.state.form,
      [question.id]: {
	question_id: question.id,
	revision_id: question.revision_id,
	score: question.scale
      }
    }
  }
});

export const SubmitAnswersFx = (dispatch, { api, link, answers }) => (
  ApiService.answerQuestionaire(link, answers).then(
    ({ data }) => dispatch(redirect(`/${link}/results`))
  )
);

export const SubmitAnswers = (s, event) => {
  event.preventDefault();

  const { link, form } = s.state;

  const answers = Object.values(form);

  return [
    s,
    [SubmitAnswersFx, {
      api: s.api,
      answers, link
    }]
  ];
}

export const displayQuestion = (answerOptions) => (question) => div({}, [
  title({ class: "mb-4" }, question.description),
  div(
    { class: "w-full flex justify-between mb-4" },
    answerOptions.map(
      option => div(
	{ class: "flex flex-col"},
	[
	  input({
	    type: "radio",
	    name: `radio_${question.id}`,
	    onchange: SelectedAnswer(question, option.id),
	    value: false
	  }),
	  p({ class: "block mt-3" }, text(option.name)),
	]
      )
    )
  )

]);

export const displayForm = (answerOptions, revision) => revision && revision.questions.map(
  displayQuestion(answerOptions)
);

export const view = s => answerPage(
  s,
  form (
    { onsubmit: SubmitAnswers },
    [
      div({} , displayForm(s.state.answer_options, s.state.revision)),
      submitbutton({}, "Send")
    ]
  )
);

view.route = '/:link';
view.routeSpec = {
  OnEnter: (state, { link }) => {
    return [
      { ...state, view, state: FormState(link, null) },
      [FormFx, { api: state.api, link }]
    ];
  }
};

export default view;
