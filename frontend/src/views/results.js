import { ul, li, a, p, span, div, text, input, form } from "@hyperapp/html";
import { halfview, title, propheading, submitbutton } from "dmd/components/index.js";
import { answerPage } from "dmd/layout/page.js";


const view = s => answerPage(
  {}
);

view.route = '/:link/results';
view.routeSpec = {
  OnEnter: (state, { link }) => ({ ...state, view })
};


export default view;
