import { ul, li, a, div, text, input, form } from "@hyperapp/html";
import { title, halfview, submitbutton } from "dmd/components/index.js";
import { page } from "dmd/layout/page.js";

export const NewClientState = () => ({
  name:  "",
  email: "",
  phone_number: "",
  demand: ""
});

export const ClientsState = (clients) => ({
  clients,
  newClient: NewClientState()
});

export const Clients = (state, clients) => ({
  ...state,
  state: ClientsState(clients)
});

export const ClientsFx = (dispatch, { api }) =>
  api.clients().then(
    ({ data }) => dispatch([Clients, data])
  ).catch(
    error => { console.error(error) }
  );

export const InjectClient = (s, client) => ({
  ...s,
  state: ClientsState(s.state.clients.concat(client))
});

export const SubmitClientFx = (dispatch, { api, client }) => {
  api.registerClient(client).then(
    ({ data }) => dispatch([InjectClient, data])
  );
};

export const SubmitClient = (state, event) => {
  event.preventDefault();
  return [state, [SubmitClientFx, {
    api: state.api,
    client: state.state.newClient
  }]];
};

const UpdateNewClientState = (state, field, value) => ({
  ...state,
  [field]: value
});

export const UpdateField = (field) => (state, event) => ({
  ...state,
  state: {
    ...state.state,
    newClient: UpdateNewClientState(state.state.newClient,
				    field,
				    event.target.value)
  }
});

const listClients = clients => (clients || []).map(
  client => li(
    a({
      href: `/admin/client/${client.id}`
    }, text(client.name))
  )
);

const _input = (value, placeholder, name) => input({
  type: "text",
  class: "w-full rounded border border-gray-200 px-10 py-6 mb-4",
  name,
  onchange: UpdateField(name),
  value,
  placeholder
});

const inputs = (newClient) =>
      [[newClient.name,'Name', 'name'],
       [newClient.email, 'E-mail', 'email'],
       [newClient.phone_number, 'Telefone', 'phone_number'],
       [newClient.demand, 'Demanda', 'demand']
      ].map(
	([value, placeholder, name]) => _input(value, placeholder, name)
      )

const view = state => {
  const { newClient, clients } = state.state;
  return page(
    state,
    halfview(
      {},
      div({}, [
	title({ class: "mb-4" }, "Client's list"),
	ul({ class: "mb-4" } , listClients(clients)),
      ]),
      div({}, [
	title({ class: "mb-4" }, "New client"),
	form(
	  { onsubmit: SubmitClient },
	  inputs(newClient).concat(
	    [
	      submitbutton({}, "Send")
	    ]
	  )
	)
      ])
    )
  );
};

view.route = '/clients';
view.routeSpec = {
  OnEnter: (state) => state.verifyAuthorization(
    state,
    () => [
      { ...state, view, state: ClientsState([]) },
      [ClientsFx, { api: state.api }]
    ]
  )
};

export default view;
