export const redirect = href => Navigate({
  href,
  options: { type: 'replace' }
});
