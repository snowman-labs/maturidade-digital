export const blockAccess = (api, path) => !Boolean(api) && path !== "/login";

export const redirectAuthed = (api, path) => Boolean(api) && path === "/login";
