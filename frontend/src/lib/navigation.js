import { Navigate } from '@mrbarrysoftware/hyperapp-router/src/withRouter/effects';

export const redirect = href => Navigate({
  href,
  options: { type: 'replace' }
});
