import axios from "axios";

const baseURL = "http://localhost:5000/api";

let instance = null;

export default class ApiService {
  static authenticate(data) {
    return axios.post(`${baseURL}/auth`, data);
  }

  static questionaireByLink(link) {
    return axios.get(`${baseURL}/questionaire/${link}`);
  }

  static answerQuestionaire(link, data) {
    return axios.post(`${baseURL}/questionaire/${link}/answers`, data);
  }

  constructor(token) {
    instance = axios.create({
      baseURL,
      headers: {'Authorization': `JWT ${token}` }
    });
  }

  call(method, url, data) {
    return instance[method](url, data).catch(
      (error) => {
	if (error.response.status === 401) {
	  const logoutEvent = new CustomEvent("logout", { detail: {} });
	  document.body.dispatchEvent(logoutEvent);
	}
	return Promise.reject(error);
      }
    );
  }

  clients() {
    return this.call('get', "/clients", null);
  }

  registerClient(client) {
    return this.call('post', "/client", client);
  }

  clientById(id) {
    return this.call('get', `/client/${id}`, null);
  }

  revisions() {
    return this.call('get', "/revisions", null);
  }

  registerRevision(revision) {
    return this.call('post', "/revision", revision);
  }

  revisionById(id) {
    return this.call('get', `/revision/${id}`, null);
  }

  generateLink(clientId, revisionId) {
    return this.call('post', `/client/${clientId}/revision/${revisionId}`, null);
  }

  answeredQuestionaireByLink(link) {
    return this.call('get', `/answers/${link}`, null);
  }
}
