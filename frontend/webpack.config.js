const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require('autoprefixer');

const { NODE_ENV } = process.env;

const isDev = NODE_ENV === "development";

const publicPath = '../public';

const fileLoaderConfig = {
  test: /\.(woff[2]?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
  loader: "file-loader",
  options: {
    esModule: false,
    outputPath: "./fonts"
  }
};

const cssLoaderConfig = {
  test: /\.css$/,
  use: [
    MiniCssExtractPlugin.loader,
    {
      loader: 'css-loader',
      options: {
	importLoaders: 1
      },
    },
    'postcss-loader'
  ]
};

module.exports = {
  entry: {
    admin: './apps/admin.js',
    form: './apps/form.js'
  },
  mode: NODE_ENV || "development",
  ...(isDev && {
    devServer: {
      historyApiFallback: true,
      hot: true,
      contentBase: path.join(__dirname, publicPath),
      compress: false,
      port: 9000,
    }
  }),
  output: {
    path: path.join(__dirname, publicPath)
  },
  module: {
    rules: [
      fileLoaderConfig,
      cssLoaderConfig
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: path.join(publicPath, "./main.css")
    })
  ],
  resolve: {
    alias: {
      "dmd": path.resolve(__dirname, 'src/')
    }
  }
};
