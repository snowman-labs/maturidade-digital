module.exports = {
  important: true,
  theme: {
    extend: {
      fontFamily: {
	"display": ["Nunito", "sans-serif"],
	"html": ["Nunito", "sans-serif"],
	"body": ["Nunito", "sans-serif"]
      },
      colors: {
	primary: "#1F67BD"
      },
      screens: {
	lg: '1086px',
	xl: '1086px'
      },
      spacing: {
	'72': '18rem',
	'84': '21rem',
	'96': '24rem',
      },
      borderRadius: {
	"frame": "20px"
      },
      boxShadow: {
	default: "10px 30px 30px 3px rgba(177,188,199,0.15)"
      },
      opacity: {
	"60": ".6",
	"75": ".75",
	"80": ".8",
	"90": ".9"
      }
    }
  }
};
