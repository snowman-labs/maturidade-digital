{ pkgs ? import <nixpkgs> {} }:
let engine = with pkgs; [python nodejs];
    dependencies = (with pkgs.python38Packages; [
      poetry
    ]);
in pkgs.mkShell {
  name = "dev-shell";
  buildInputs = engine ++ dependencies;
}

